<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'ItemsController@index')->name('home');
Route::get('/{item}', 'ItemsController@show');
Route::post('/{item}', 'ItemsController@update')->name('item.update');
Route::post('/{item}/delete', 'ItemsController@destroy')->name('item.delete');

