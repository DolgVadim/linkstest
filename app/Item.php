<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;

class Item extends Model
{
    protected $fillable = ['name', 'key'];
    protected $perPage = 5;

    /**
     * Validator for  \App\Item
     *
     * @param $fields
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public static function validator($fields)
    {
        return Validator::make(
            $fields,
            [
                'name' => 'max:255',
                'key' => 'required|max:25'
            ]);
    }

    /**
     * Saves items history
     *
     * @param array $attributes
     * @param array $options
     * @return bool
     */
    public function update(array $attributes = [], array $options = [])
    {
        $this->history()->create($this->toArray());
        return parent::update($attributes, $options);
    }

    public function history()
    {
        return $this->hasMany(ItemHistory::class);
    }
}
