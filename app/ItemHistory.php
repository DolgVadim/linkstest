<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ItemHistory extends Model
{
    protected $table = 'item_history';
    public $timestamps = false;
    protected $fillable = ['name', 'key'];

    public function item()
    {
        return $this->belongsTo(Item::class);
    }
}
