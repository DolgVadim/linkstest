<?php

namespace App\Http\Middleware;

use App\Http\Controllers\Helpers\JsonResponse;
use Closure;

class ApiToken
{
    /**
     * Check api token.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->header('api-token') != env('API_TOKEN')) {
            return JsonResponse::response(401, 'not authorized', 'To authorize use http header Api-Token = ' . env('API_TOKEN'));
        }

        return $next($request);
    }
}
