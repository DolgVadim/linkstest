<?php

namespace App\Http\Controllers\Helpers;


class JsonResponse
{
    /**
     * Generic json response
     *
     * @param $status
     * @param string $code
     * @param string $message
     * @param array $data
     * @param string $dataKey
     * @return \Illuminate\Http\JsonResponse
     */
    public static function response($status, $code = "ok", $message = '', $data = [], $dataKey = 'data')
    {
        $response['code'] = $code;

        if (!empty($message))
            $response['message'] = $message;

        if (!empty($data)) {
            $response[$dataKey] = $data;
        }

        return response()->json($response, $status);
    }


    /**
     * 200 status
     *
     * @param string $message
     * @param array $data
     * @return \Illuminate\Http\JsonResponse
     */
    public static function ok($message = '', $data = [])
    {
        return self::response(200, 'ok', $message, $data);
    }

    /**
     * 404 status
     *
     * @param string $message
     * @return \Illuminate\Http\JsonResponse
     */
    public static function notFound($message = '')
    {
        return self::response(404, 'not found', $message);

    }

    /**
     * 400 status
     *
     * @param string $message
     * @param array $errors
     * @return \Illuminate\Http\JsonResponse
     */
    public static function bad($message = '', $errors = [])
    {
        return self::response(400, 'bad request', $message, $errors, 'errors');

    }

    /**
     * 201 status
     *
     * @param string $message
     * @return \Illuminate\Http\JsonResponse
     */
    public static function created($message = '')
    {
        return self::response(201, 'created', $message);

    }

    /**
     * 202 status
     *
     * @param string $message
     * @return \Illuminate\Http\JsonResponse
     */
    public static function updated($message = '')
    {
        return self::response(202, 'accepted', $message);

    }
}
