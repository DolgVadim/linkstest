<?php

namespace App\Http\Controllers;

use App\Item;
use App\Http\Resources\ItemCollection;
use App\Http\Controllers\Helpers\JsonResponse;
use App\Http\Resources\Item as ItemResource;
use Illuminate\Http\Request;

class ItemsApiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        return new ItemCollection(Item::paginate());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $validation = Item::validator($request->all());
        if ($validation->fails())
            return JsonResponse::bad('validation fails', $validation->errors()->toArray());

        $item = new Item($request->all());
        $item->save();
        return JsonResponse::ok('Item added', $item->toArray());
    }

    /**
     * Display the specified resource.
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $item = Item::find($id);
        if (!$item)
            return JsonResponse::notFound('No item with id ' . $id);

        return (new ItemResource($item))->response();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
        $item = Item::find($id);
        if (!$item)
            return JsonResponse::notFound('No item with id ' . $id);

        $validation = Item::validator($request->all());
        if ($validation->fails())
            return JsonResponse::bad('validation fails', $validation->errors()->toArray());

        $item->update($request->all());

        return JsonResponse::ok('Item updated', $item->toArray());

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy($id)
    {
        $item = Item::find($id);
        if (!$item)
            return JsonResponse::notFound('No item with id ' . $id);;

        $item->delete();

        return JsonResponse::ok('Item deleted');

    }
}
