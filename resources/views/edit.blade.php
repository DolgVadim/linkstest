@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <h2>Item</h2>
                <div class="card">
                    <div class="card-header">edit #{{$item->id}}</div>
                    <div class="card-body">
                        <form class="mb-2" method="post" action="/{{$item->id}}">
                            @csrf
                            <div class="form-group">
                                <label for="name">name</label>
                                <input type="text" class="form-control" id="name" name="name" value="{{$item->name}}">
                            </div>
                            <div class="form-group">
                                <label for="key">key</label>
                                <input type="text" class="form-control" id="key" name="key" value="{{$item->key}}">
                            </div>
                            <input class="btn btn-primary" type="submit" value="Save">
                        </form>
                        <form method="post" action="/{{$item->id}}/delete">
                            @csrf
                            <input type="submit" class="btn btn-danger" value="Delete">
                        </form>
                    </div>
                </div>
            </div>
            @php($history = $item->history()->orderBy('id','desc')->take(3)->get())
            @if(!empty($history))
                <div class="col-md-4">
                    <h3>History (last three)</h3>
                    @foreach($history as $record)
                        <div class="card mb-5">
                            <div class="card-header">{{$record->updated_at}}</div>
                            <div class="card-body">
                                <div class="mb-3">name: {{$record->name}}</div>
                                <div class="mb-3">key: {{$record->key}}</div>
                            </div>
                        </div>
                    @endforeach
                </div>
            @endif
        </div>
    </div>
@endsection
