@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            @foreach($items as $item)
                <div class="col-md-4 mb-5">
                    <div class="card">
                        <div class="card-header">id - {{$item->id}}</div>
                        <div class="card-body">
                            <div class="mb-3">name: {{$item->name}}</div>
                            <div class="mb-3">key: {{$item->key}}</div>
                            <a href="/{{$item->id}}" class="btn btn-primary">Edit</a>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@endsection
