<?php

namespace Tests\Unit\Helpers;


trait ItemTrait
{
    /**
     * Get Item's random attributes
     *
     * @return array
     */
    public function getStub()
    {
        return [
            'name' => $this->fake->name(),
            'key' => substr($this->fake->md5(), 0, 25)
        ];
    }
}