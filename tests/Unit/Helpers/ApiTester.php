<?php

namespace Tests\Unit\Helpers;

use Illuminate\Support\Facades\Artisan;
use Tests\TestCase;
use Faker\Factory;

abstract class ApiTester extends TestCase
{
    protected $fake;
    protected $count = 1;

    /**
     * ApiTester constructor with injected Faker
     */
    public function __construct()
    {
        parent::__construct();

        $this->fake = Factory::create();
    }

    public function setUp()
    {
        parent::setUp();
        Artisan::call('migrate');
    }

    /**
     * Check multiple keys
     *
     * @param array $array
     * @param array $keys
     */
    public function assertArrayHasKeys(array $array, array $keys)
    {

        foreach ($keys as $key) {
            $this->assertArrayHasKey($key, $array);
        }

    }

    /**
     * Create new model
     *
     * @param $type
     * @param $data
     */
    public function make($type, $data)
    {
        $type::create($data);
    }


}