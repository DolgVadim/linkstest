<?php

namespace Tests\Unit;

use Tests\Unit\Helpers\ApiTester;
use Tests\Unit\Helpers\ItemTrait;

class ItemsApiTest extends ApiTester
{
    use ItemTrait;

    /**
     * @test
     */
    public function fetch_all_items_with_pagination()
    {
        foreach (range(1, 10) as $index) {
            $this->make('\App\Item', $this->getStub());
        }

        $response = $this->getJson('api/items');

        $data = json_decode($response->getContent(), true);

        $this->assertArrayHasKeys($data, ['code', 'data', 'links', 'meta']);

        $this->assertEquals('ok', $data['code']);

        $this->assertEquals(10, $data['meta']['total']);

        $response->assertStatus(200);
    }

    /**
     * @test
     */
    public function fetch_single_item()
    {
        $this->make('\App\Item', $this->getStub());

        $response = $this->getJson('api/items/1');

        $data = json_decode($response->getContent(), true);

        $this->assertArrayHasKeys($data, ['code', 'data']);

        $this->assertEquals('ok', $data['code']);

        $response->assertStatus(200);

    }

    /**
     * @test
     */
    public function create_item()
    {
        $response = $this->postJson('api/items', $this->getStub());

        $data = json_decode($response->getContent(), true);

        $this->assertArrayHasKeys($data, ['code', 'data']);

        $this->assertArrayHasKeys($data['data'], ['id', 'name', 'key']);

        $this->assertEquals('ok', $data['code']);

        $response->assertStatus(200);
    }

    /**
     * @test
     */
    public function update_item()
    {
        $this->make('\App\Item', $this->getStub());

        $newData = $modelData = $this->getStub();

        $response = $this->putJson('api/items/1', $newData);

        $data = json_decode($response->getContent(), true);

        $this->assertArrayHasKeys($data, ['code', 'data']);

        $this->assertArrayHasKeys($data['data'], ['id', 'name', 'key']);

        $this->assertEquals('ok', $data['code']);

        $this->assertEquals($newData['name'], $data['data']['name']);

        $this->assertEquals($newData['key'], $data['data']['key']);

        $response->assertStatus(200);

    }

    /**
     * @test
     */
    public function delete_item()
    {
        $this->make('\App\Item', $this->getStub());

        $response = $this->deleteJson('api/items/1');

        $data = json_decode($response->getContent(), true);

        $this->assertArrayHasKeys($data, ['code', 'message']);

        $this->assertEquals('ok', $data['code']);

        $response->assertStatus(200);

        $items = \App\Item::all();

        $this->assertEquals([], $items->toArray());
    }

    /**
     * @test
     */
    public function get_404_on_fetch_single_item()
    {
        $response = $this->getJson('api/items/x');

        $data = json_decode($response->getContent(), true);

        $this->assertArrayHasKeys($data, ['code', 'message']);

        $response->assertStatus(404);
    }

    /**
     * @test
     */
    public function get_404_on_update_item()
    {
        $response = $this->putJson('api/items/x');

        $data = json_decode($response->getContent(), true);

        $this->assertArrayHasKeys($data, ['code', 'message']);

        $response->assertStatus(404);
    }

    /**
     * @test
     */
    public function get_404_on_delete_item()
    {
        $response = $this->deleteJson('api/items/x');

        $data = json_decode($response->getContent(), true);

        $this->assertArrayHasKeys($data, ['code', 'message']);

        $response->assertStatus(404);
    }

    /**
     * @test
     */
    public function get_400_validation_fail_on_create()
    {
        $response = $this->postJson('api/items');

        $data = json_decode($response->getContent(), true);


        $this->assertArrayHasKeys($data, ['code', 'message', 'errors']);

        $response->assertStatus(400);
    }

    /**
     * @test
     */
    public function get_400_validation_fail_on_update()
    {
        $this->make('\App\Item', $this->getStub());

        $response = $this->putJson('api/items/1', []);

        $data = json_decode($response->getContent(), true);

        $this->assertArrayHasKeys($data, ['code', 'message', 'errors']);

        $response->assertStatus(400);
    }

}

