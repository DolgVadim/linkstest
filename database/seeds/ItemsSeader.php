<?php

use Illuminate\Database\Seeder;

use Faker\Factory;


class ItemsSeader extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();

        foreach (range(0,10) as $index){
            \App\Item::create([
                'name'=>$faker->name(),
                'key'=>substr($faker->md5(),0,25)
            ]);
        }

    }
}
